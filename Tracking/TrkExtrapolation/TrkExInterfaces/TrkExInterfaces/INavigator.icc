/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/*
 * Implementation of interfaces without Gaudi Context
 * in terms of the Gaudi Context aware ones
 */
namespace Trk {
/** INavigator interface method - getting the next BoundarySurface not knowing
 * the Volume*/
inline const BoundarySurface<TrackingVolume>*
INavigator::nextBoundarySurface(const IPropagator& prop,
                                const TrackParameters& parms,
                                PropDirection dir) const
{
  return nextBoundarySurface(Gaudi::Hive::currentContext(), prop, parms, dir);
}

/** INavigator interface method - getting the next BoundarySurface when
 * knowing the Volume*/
inline const BoundarySurface<TrackingVolume>*
INavigator::nextBoundarySurface(const IPropagator& prop,
                                const TrackParameters& parms,
                                PropDirection dir,
                                const TrackingVolume& vol) const
{

  return nextBoundarySurface(
    Gaudi::Hive::currentContext(), prop, parms, dir, vol);
}

/** INavigator interface method - - getting the next Volume and the parameter
 * for the next Navigation */
inline NavigationCell
INavigator::nextTrackingVolume(const IPropagator& prop,
                               const TrackParameters& parms,
                               PropDirection dir,
                               const TrackingVolume& vol) const
{

  return nextTrackingVolume(
    Gaudi::Hive::currentContext(),prop,parms, dir, vol);
}

/** INavigator interface method - getting the next Volume and the parameter
  for the next Navigation
  - contains full loop over volume boundaries
*/
inline NavigationCell
INavigator::nextDenseTrackingVolume(const IPropagator& prop,
                                    const TrackParameters& parms,
                                    const Surface* destination,
                                    PropDirection dir,
                                    ParticleHypothesis particle,
                                    const TrackingVolume& vol,
                                    double& path) const
{

  return nextDenseTrackingVolume(Gaudi::Hive::currentContext(),
                                 prop,
                                 parms,
                                 destination,
                                 dir,
                                 particle,
                                 vol,
                                 path);
}


}
